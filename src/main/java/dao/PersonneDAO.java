package DAO;

import metier.Personne;

import java.sql.Connection;
import java.util.ArrayList;

public class PersonneDAO extends DAO<Personne>{

    public PersonneDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Personne getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Personne> getAll() {
        return null;
    }

    @Override
    public boolean insert(Personne objet) {
        return false;
    }

    @Override
    public boolean update(Personne objet) {
        return false;
    }

    @Override
    public boolean delete(Personne objet) {
        return false;
    }
}
