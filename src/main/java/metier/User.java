package metier;

public class User {
    private String login;
    private String passwordHach;

    public User(String login, String passwordHach) {
        this.login = login;
        this.passwordHach = passwordHach;
    }

    public String getLogin() {
        return login;
    }

    public User setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPasswordHach() {
        return passwordHach;
    }

    public User setPasswordHach(String passwordHach) {
        this.passwordHach = passwordHach;
        return this;
    }
}
